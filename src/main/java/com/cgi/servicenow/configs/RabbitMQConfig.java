package com.cgi.servicenow.configs;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    @Bean
    public Queue messageQueue() {
        return new Queue("messages", false);
    }

    @Bean
    public Queue ticketsQueue() {
        return new Queue("servicenow_tickets", false);
    }
}
