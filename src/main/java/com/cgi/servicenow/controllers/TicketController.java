package com.cgi.servicenow.controllers;

import com.cgi.servicenow.api.ServiceNowTicketDto;
import com.cgi.servicenow.entities.Ticket;
import com.cgi.servicenow.services.TicketService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/tickets")
public class TicketController {
    private TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PostMapping
    @ApiOperation(value = "Saves ticket",
            notes = "Provide saving operation of specific ticket",
            response = Ticket.class)
    public ResponseEntity<ServiceNowTicketDto> saveTicket(@ApiParam(value = "Ticket object for saving",
            required = true) @Valid ServiceNowTicketDto ticketDto) {
        return new ResponseEntity<>(ticketService.saveTicket(ticketDto), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Finds ticket by id",
            notes = "Provide an id to look up specific ticket",
            response = Ticket.class)
    public ResponseEntity<ServiceNowTicketDto> getTicketById(@ApiParam(value = "Id value for retrieving the ticket",
            required = true) @PathVariable Long id) {
        return new ResponseEntity<>(ticketService.getTicketById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/name", params = "value", method = RequestMethod.GET)
    @ApiOperation(value = "Finds ticket by name",
            notes = "Provide a name to look up specific ticket",
            response = Ticket.class)
    public ResponseEntity<ServiceNowTicketDto> getTicketByName(@ApiParam(value = "Name string for retrieving the ticket",
            required = true) @RequestParam(value = "value") String name) {
        return new ResponseEntity<>(ticketService.getTicketByName(name), HttpStatus.OK);
    }

    @RequestMapping(params = {"offset", "limit"}, method = RequestMethod.GET)
    @ApiOperation(value = "Finds all tickets using pagination",
            notes = "Provide an offset and limit for listing all tickets using pagination",
            response = Ticket.class)
    public ResponseEntity<List<ServiceNowTicketDto>> getAllTickets(
            @ApiParam(value = "Offset value for show current page",
                    required = true) @RequestParam int offset,
            @ApiParam(value = "Limit value for set ticket's limit on the page",
                    required = true) @RequestParam int limit) {
        return new ResponseEntity<>(ticketService.getAllTickets(offset, limit), HttpStatus.OK);
    }
}
