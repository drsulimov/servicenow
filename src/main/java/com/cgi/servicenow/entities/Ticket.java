package com.cgi.servicenow.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.time.Instant;

@Getter
@Setter
@ToString
@Entity
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String email;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    private Timestamp creationDatetime;

    public Ticket() {
        this.creationDatetime = new Timestamp(Instant.now().toEpochMilli());
    }

    public Ticket(Long id,
                  String name,
                  String email,
                  Long idPersonCreator,
                  Long idPersonAssigned,
                  Timestamp creationDatetime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDatetime = creationDatetime;
    }
}