package com.cgi.servicenow.repositories;

import com.cgi.servicenow.entities.Ticket;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Optional<Ticket> getTicketById(Long id);

    Optional<Ticket> getTicketByName(String name);

    @Query(value = "SELECT t FROM Ticket t ORDER BY t.id ASC")
    List<Ticket> getAllTickets(Pageable pageable);
}
