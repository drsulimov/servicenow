package com.cgi.servicenow.api;

import com.cgi.servicenow.handlers.ValidEmail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

@Getter
@Setter
@ToString
@ApiModel(description = "Details about the ticket")
public class ServiceNowTicketDto implements Serializable {
    @ApiModelProperty(notes = "The unique id of the ticket")
    private Long id;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[A-Z]+[0-9]{5}")
    @ApiModelProperty(notes = "The ticket's name")
    private String name;

    @NotNull
    @NotEmpty
    @ValidEmail
    @Length(max = 100)
    @ApiModelProperty(notes = "The person's email")
    private String email;

    @NotNull
    @ApiModelProperty(notes = "The person's id who created the ticket")
    private Long idPersonCreator;

    @NotNull
    @ApiModelProperty(notes = "The person's id who assigned to ticket")
    private Long idPersonAssigned;

    @ApiModelProperty(notes = "Date and time when ticket was created")
    private Timestamp creationDatetime;

    public ServiceNowTicketDto() {
        this.creationDatetime = new Timestamp(Instant.now().toEpochMilli());
    }

    public ServiceNowTicketDto(Long id,
                               String name,
                               String email,
                               Long idPersonCreator,
                               Long idPersonAssigned,
                               Timestamp creationDatetime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDatetime = creationDatetime;
    }
}
