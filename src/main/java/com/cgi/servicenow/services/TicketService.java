package com.cgi.servicenow.services;

import com.cgi.servicenow.api.ServiceNowTicketDto;
import com.cgi.servicenow.entities.Ticket;
import com.cgi.servicenow.exceptions.InvalidInputException;
import com.cgi.servicenow.exceptions.ObjectNotFoundException;
import com.cgi.servicenow.mappers.BeanMapping;
import com.cgi.servicenow.repositories.TicketRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TicketService {
    private TicketRepository ticketRepository;
    private RabbitTemplate rabbitTemplate;
    private BeanMapping beanMapping;

    @Autowired
    public TicketService(TicketRepository ticketRepository,
                         BeanMapping beanMapping,
                         RabbitTemplate rabbitTemplate) {
        this.ticketRepository = ticketRepository;
        this.beanMapping = beanMapping;
        this.rabbitTemplate = rabbitTemplate;
    }

    public ServiceNowTicketDto saveTicket(ServiceNowTicketDto ticketShadow) {
        Ticket ticket = beanMapping.mapTo(ticketShadow, Ticket.class);
        Ticket savedTicket = ticketRepository.save(ticket);
        rabbitTemplate.convertAndSend("messages", "Ticket " + ticket.getName() + " was created");
        rabbitTemplate.convertAndSend("servicenow_tickets", ticketShadow);
        return beanMapping.mapTo(savedTicket, ServiceNowTicketDto.class);
    }

    public ServiceNowTicketDto getTicketById(Long id) {
        Ticket ticket = ticketRepository.getTicketById(id).orElseThrow(() -> new ObjectNotFoundException("Ticket not found"));
        return beanMapping.mapTo(ticket, ServiceNowTicketDto.class);
    }

    public ServiceNowTicketDto getTicketByName(String name) {
        Ticket ticket = ticketRepository.getTicketByName(name).orElseThrow(() -> new ObjectNotFoundException("Ticket not found"));
        return beanMapping.mapTo(ticket, ServiceNowTicketDto.class);
    }

    public List<ServiceNowTicketDto> getAllTickets(int offset, int limit) {
        if (offset < 0 || limit <= 0) throw new InvalidInputException("Offset and limit must be greater than 0");
        return beanMapping.mapTo(ticketRepository.getAllTickets(PageRequest.of(offset, limit)), ServiceNowTicketDto.class);
    }
}
