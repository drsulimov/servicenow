package com.cgi.servicenow.handlers;

import com.cgi.servicenow.exceptions.InvalidInputException;
import com.cgi.servicenow.exceptions.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class RestExceptionsHandler {
    private static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<ApiError> invalidInputException(InvalidInputException invalidInput,
                                                          final HttpServletRequest request) {
        ApiError apiError = new ApiError(400,
                invalidInput.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<ApiError> objectNotFoundException(ObjectNotFoundException objectNotFound,
                                                            final HttpServletRequest request) {
        ApiError apiError = new ApiError(404,
                objectNotFound.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }
}
